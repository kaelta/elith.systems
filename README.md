# Elith.systems

Elith.Systems is a rewrite of the completed version of my old codebase of the same name.
However, it is now a portfolio project rather than the recreated production build of my former business.

This serves to demonstrate knowledge of CMS, APIs and Next.JS.

## Stack

### Frontend

- **Next.JS**           - Frontend framework
- **NextUI**            - UI library
- **Apollo GraphQL**    - Endpoints

### Backend

- **Strapi**        - Headless CMS
- **PostgreSQL**    - Backend Database

### Deployment

- **Vercel**        - Deployment platform
- **Strapi Cloud**  - CMS platform

