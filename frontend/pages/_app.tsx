import "@/styles/globals.css";
import Layout from "@/components/Layout";
import Providers from "@/components/Providers";
import React from "react";

export default function App({Component, pageProps}: any): React.ReactElement {
    return (
        <Providers>
            <Layout>
				<Component {...pageProps} />
            </Layout>
        </Providers>
    );
}
