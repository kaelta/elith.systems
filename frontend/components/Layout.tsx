import Head from "next/head";
import NavbarComponent from "@/components/Navbar";
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export default function Layout(props: { children: React.ReactNode }): React.ReactElement {
  const title = "Welcome to Elith.Systems";

  return (
    <div className={inter.className}>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <NavbarComponent />
      <div className="container mx-auto px-4">{props.children}</div>
    </div>
  );
}
