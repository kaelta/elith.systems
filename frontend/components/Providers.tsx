import React, {ReactElement} from 'react';
import {NextUIProvider} from "@nextui-org/react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import {ComponentPreviews, useInitial} from "@/dev";
import {DevSupport} from "@react-buddy/ide-toolbox-next";

const API_URL = process.env.STRAPI_URL || "http://127.0.0.1:1337";

export const client = new ApolloClient({
	uri: `${API_URL}/graphql`,
	cache: new InMemoryCache(),
	defaultOptions: {
		mutate: {
			errorPolicy: "all",
		},
		query: {
			errorPolicy: "all",
		},
	},
});


export default function Providers(props: {children: ReactElement}): ReactElement {
    return (
        <NextUIProvider>
			<ApolloProvider client={client}>
				<DevSupport ComponentPreviews={ComponentPreviews} useInitialHook={useInitial}
				>
            		{props.children}
				</DevSupport>
			</ApolloProvider>
        </NextUIProvider>
    )
}
