import { gql, useQuery } from "@apollo/client";
import Link from "next/link";
import Image from "next/image";
import {Skeleton} from "@nextui-org/react";

const QUERY = gql`
  {
  products {
    data {
      id
      attributes {
        Name
        Description
        Images {
          data {
            id
            attributes {
              name
              url
            }
          }
        }
      }
    }
  }
}

`;

export function ProductCard({data}: any) {
    return (
		<div className="w-full md:w-1/2 lg:w-1/3 p-4">
			<div className="h-full bg-gray-100 rounded-2xl">
				<Image
					className="w-full rounded-2xl"
					height={300}
					width={300}
					src={`${process.env.STRAPI_URL || "http://localhost:1337"}${
						data.attributes.image.data[0].attributes.url
					}`}
					alt=""
				/>
				<div className="p-8">
					<h3 className="mb-3 font-heading text-xl text-gray-900 hover:text-gray-700 group-hover:underline font-black">
						{data.attributes.name}
					</h3>
					<p className="text-sm text-gray-500 font-bold">
						{data.attributes.description}
					</p>
					<div className="flex flex-wrap md:justify-center -m-2">
						<div className="w-full md:w-auto p-2 my-6">
							<Link
								className="block w-full px-12 py-3.5 text-lg text-center text-white font-bold bg-gray-900 hover:bg-gray-800 focus:ring-4 focus:ring-gray-600 rounded-full"
								href={`/restaurant/${data.id}`}
							>
								View
							</Link>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

function ProductList(props: any) {
	const {loading, error, data} = useQuery(QUERY);

	if (error) return "Error loading products!";
	if (loading) return <Skeleton className="flex rounded-full w-12 h-12"/>;

	if (data.products.data && data.products.data.length) {
		const searchQuery = data.products.data.filter((query: any) =>
			query.attributes.name.toLowerCase().includes(props.query.toLowerCase())
		);

		if (searchQuery.length != 0) {
			return (
				<div className="py-16 px-8 bg-white rounded-3xl">
					<div className="max-w-7xl mx-auto">
						<div className="flex flex-wrap -m-4 mb-6">
							{searchQuery.map((response: any) => {
								return <ProductCard key={response.id} data={response}/>;
							})}
						</div>
					</div>
				</div>
			);
		} else {
			return <h1>No Products Found! :(</h1>;
		}
	}
	return <h5>Products</h5>;
}

export default ProductList;
